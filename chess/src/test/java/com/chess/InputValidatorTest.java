package com.chess;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class InputValidatorTest {

    private static String[] columnArray = new String[] {"A", "B", "C", "D", "E", "F", "G", "H"};

    private static String[] rowArray = new String[] {"1", "2", "3", "4", "5", "6", "7", "8"};

    @Test
    public void testValidations() {
        InputValidator validator = new InputValidator();
        Chessboard chessboard = new Chessboard(columnArray, rowArray, null);
        ChessApplication.prepareChessBoard(chessboard);
        Map<Integer, Object[]> userInputs = new LinkedHashMap<>();
        userInputs.put(0, new Object[] {"Queen", false});
        userInputs.put(1, new Object[] {"kingd5", false});
        userInputs.put(2, new Object[] {"abcd 1", false});
        userInputs.put(3, new Object[] {"Queen 22", false});
        userInputs.put(4, new Object[] {"Queen 2 2", false});
        userInputs.put(5, new Object[] {"Queen A", false});
        userInputs.put(6, new Object[] {" Queen A8", false});
        userInputs.put(7, new Object[] {"Queen A8", true});
        userInputs.put(8, new Object[] {"queen A8", true});
        userInputs.put(9, new Object[] {"QUEEN A8", true});
        userInputs.put(10, new Object[] {"Queen a8", true});
        for (int i = 0; i < userInputs.size(); i++) {
            String userInput = (String) userInputs.get(i)[0];
            boolean expectedOutput = (boolean) userInputs.get(i)[1];
            boolean actualOutput = validator.validateInput(userInput.split(" "), chessboard);
            System.out.println("userInput:"+userInput+" | expectedoutput: "+expectedOutput+" | actualOutput: "+actualOutput);
            Assert.assertEquals(expectedOutput, actualOutput);
        }
    }
}