package com.chess;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.util.Arrays;
import org.junit.Assert;
import org.junit.Test;

public class ChessApplicationTests {

    private static String[] columnArray = new String[] {"A", "B", "C", "D", "E", "F", "G", "H"};

    private static String[] rowArray = new String[] {"1", "2", "3", "4", "5", "6", "7", "8"};

    @Test
    public void testHappyPathScenarios() {
        Chessboard chessboard = new Chessboard(columnArray, rowArray, null);
        ChessApplication.prepareChessBoard(chessboard);
        Map<Integer, Object[]> userInputs = new LinkedHashMap<>();
        userInputs.put(0, new Object[] {"King D5", Arrays.asList(new String[] {"C4", "D4", "E4", "C5", "E5", "C6", "D6", "E6"})});
        userInputs.put(1, new Object[] {"King h1", Arrays.asList(new String[] {"G1", "G2", "H2"})});
        userInputs.put(2, new Object[] {"queen h1", Arrays.asList(new String[] {"A1", "B1", "C1", "D1", "E1", "F1", "G1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "A8", "B7", "C6", "D5", "E4", "F3", "G2"})});
        userInputs.put(3, new Object[] {"Bishop E4", Arrays.asList(new String[] {"D5", "F3", "D3", "F5", "C6", "G2", "C2", "G6", "B7", "H1", "B1", "H7", "A8"})});
        userInputs.put(4, new Object[] {"Horse D6", Arrays.asList(new String[] {"C4", "E4", "C8", "E8", "B5", "B7", "F5", "F7"})});
        userInputs.put(5, new Object[] {"Rook A1", Arrays.asList(new String[] {"B1", "C1", "D1", "E1", "F1", "G1", "H1", "A2", "A3", "A4", "A5", "A6", "A7", "A8"})});
        userInputs.put(6, new Object[] {"Pawn G6", Arrays.asList(new String[] {"G5","G7"})});
        userInputs.put(7, new Object[] {"King", null});
        userInputs.put(8, new Object[] {"kingd5", null});
        userInputs.put(9, new Object[] {"abcd 1", null});
        for (int i = 0; i < userInputs.size(); i++) {
            String userInput = (String) userInputs.get(i)[0];
            List<String> expectedOutput = (List<String>) userInputs.get(i)[1];
            List<String> moves = ChessApplication.identifyAndPrintPossibleMoves(userInput, chessboard);
            System.out.println("userInput:"+userInput+" expectedoutput: "+expectedOutput);
            Assert.assertEquals(expectedOutput, moves);
        }
    }
}