package com.chess;

import java.util.LinkedList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class PossibleMovesIdentifier {
    
    protected static List<String> fetchPossibleMoves(ReferencePieceTypes referencePieceTypes, String position, Chessboard chessBoard) {
        switch (referencePieceTypes) {
            case KING:
                return identifyMovesForKing(position,chessBoard);
            case QUEEN:
                return identifyMovesForQueen(position,chessBoard);
            case BISHOP:
                return identifyMovesForBishop(position,chessBoard);
            case HORSE:
                return identifyMovesForHorse(position,chessBoard);
            case ROOK:
                return identifyMovesForRook(position,chessBoard);
            case PAWN:
                return identifyMovesForPawn(position,chessBoard);
            default:
                System.out.println("Invalid piece type provided");
        }
        return null;
    }

    private static List<String> identifyMovesForPawn(String position, Chessboard chessBoard) {
        List<String> listOfPossibleMoves = new LinkedList<String>();
        int rowIndex = fetchIndexFromIntegerString(position.substring(1));
        int columnIndex = fetchIndexFromAlphabet(position.substring(0, 1),chessBoard.getColumnArray());
        int[] possibleMovesArr = new int[] {-1, 0, 1};

        for (int i = 0; i < possibleMovesArr.length; i++) {
            try {
                int row = rowIndex + possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[row][columnIndex];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }
        return listOfPossibleMoves;
    }

    private static List<String>  identifyMovesForRook(String position, Chessboard chessBoard) {
        List<String> listOfPossibleMoves = new LinkedList<String>();
        int rowIndex = fetchIndexFromIntegerString(position.substring(1));
        int columnIndex = fetchIndexFromAlphabet(position.substring(0, 1),chessBoard.getColumnArray());
        int[] possibleMovesArr = IntStream.range(-7, 8).toArray();
        
        for(int i=0;i<possibleMovesArr.length;i++){
            try {
                int column = columnIndex + possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[rowIndex][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }
        
        for(int i=0;i<possibleMovesArr.length;i++){
            try {
                int row = rowIndex + possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[row][columnIndex];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }
        return listOfPossibleMoves;
    }

    private static List<String> identifyMovesForHorse(String position, Chessboard chessBoard) {
        List<String> listOfPossibleMoves = new LinkedList<String>();
        int rowIndex = fetchIndexFromIntegerString(position.substring(1));
        int columnIndex = fetchIndexFromAlphabet(position.substring(0, 1),chessBoard.getColumnArray());
        int[] maxJumps = new int[] {-2, 2};
        int[] minJumps = new int[] {-1, 1};

        for (int i = 0; i < 2; i++) {
            try {
                int row = rowIndex + maxJumps[i];
                for (int j = 0; j < 2; j++) {
                    try {
                        int column = columnIndex + minJumps[j];
                        String move = chessBoard.getChessboardArray()[row][column];
                        if (!move.equalsIgnoreCase(position)) {
                            listOfPossibleMoves.add(move);
                        }
                    }
                    catch (ArrayIndexOutOfBoundsException e) {
                    }
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
            }
        }
        
        for (int i = 0; i < 2; i++) {
            try {
                int column = columnIndex + maxJumps[i];
                for (int j = 0; j < 2; j++) {
                    try {
                        int row = rowIndex + minJumps[j];
                        String move = chessBoard.getChessboardArray()[row][column];
                        if (!move.equalsIgnoreCase(position)) {
                            listOfPossibleMoves.add(move);
                        }
                    }
                    catch (ArrayIndexOutOfBoundsException e) {
                    }
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
            }
        }
            
        return listOfPossibleMoves;
    }

    private static List<String>  identifyMovesForBishop(String position, Chessboard chessBoard) {
        List<String> listOfPossibleMoves = new LinkedList<String>();
        int rowIndex = fetchIndexFromIntegerString(position.substring(1));
        int columnIndex = fetchIndexFromAlphabet(position.substring(0, 1),chessBoard.getColumnArray());
        for(int i=0;i<8;i++){
            try {
                int row = rowIndex + i;
                int column = columnIndex - i;
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
            
            try {
                int row = rowIndex - i;
                int column = columnIndex + i;
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
            
            try {
                int row = rowIndex - i;
                int column = columnIndex - i;
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
            
            try {
                int row = rowIndex + i;
                int column = columnIndex + i;
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }
        
        return listOfPossibleMoves;
    }

    private static List<String>  identifyMovesForQueen(String position, Chessboard chessBoard) {
        List<String> listOfPossibleMoves = new LinkedList<String>();
        int rowIndex = fetchIndexFromIntegerString(position.substring(1));
        int columnIndex = fetchIndexFromAlphabet(position.substring(0, 1),chessBoard.getColumnArray());
        int[] possibleMovesArr = IntStream.range(-7, 8).toArray();
        
        for(int i=0;i<possibleMovesArr.length;i++){
            try {
                int column = columnIndex + possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[rowIndex][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }
        
        for(int i=0;i<possibleMovesArr.length;i++){
            try {
                int row = rowIndex + possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[row][columnIndex];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }
        
        for(int i=0;i<8;i++){
            try {
                int row = rowIndex + possibleMovesArr[i];
                int column = columnIndex - possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
            
            try {
                int row = rowIndex - possibleMovesArr[i];
                int column = columnIndex + possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
            
            try {
                int row = rowIndex - possibleMovesArr[i];
                int column = columnIndex - possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
            
            try {
                int row = rowIndex + possibleMovesArr[i];
                int column = columnIndex + possibleMovesArr[i];
                String move = chessBoard.getChessboardArray()[row][column];
                if (!move.equalsIgnoreCase(position)) {
                    listOfPossibleMoves.add(move);
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {}
        }
        
        return listOfPossibleMoves;
    }

    private static List<String> identifyMovesForKing(String position, Chessboard chessBoard) {
        List<String> listOfPossibleMoves = new LinkedList<String>();
        int rowIndex = fetchIndexFromIntegerString(position.substring(1));
        int columnIndex = fetchIndexFromAlphabet(position.substring(0, 1),chessBoard.getColumnArray());
        int[] possibleMovesArr = new int[] {-1, 0, 1};
        for (int i = 0; i < possibleMovesArr.length; i++) {
            int row = rowIndex + possibleMovesArr[i];
            for (int j = 0; j < possibleMovesArr.length; j++) {
                try {
                    int column = columnIndex + possibleMovesArr[j];
                    String move = chessBoard.getChessboardArray()[row][column];
                    if (!move.equalsIgnoreCase(position)) {
                        listOfPossibleMoves.add(move);
                    }
                }
                catch (ArrayIndexOutOfBoundsException e) {

                }
            }
        }
        return listOfPossibleMoves;
    }

    private static int fetchIndexFromIntegerString(String substring) {
        return Integer.valueOf(substring)-1;
    }

    private static int fetchIndexFromAlphabet(String alphabet,String[] columnArray) {
       OptionalInt index = IntStream.range(0, columnArray.length).filter(i -> columnArray[i].equalsIgnoreCase(alphabet)).findFirst();
        return index.getAsInt();
    }
}