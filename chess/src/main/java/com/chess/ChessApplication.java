package com.chess;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChessApplication {

    private static final String EMPTY_STRING = "";
    private static final String REGEX_SPACE = " ";
    private static String[] columnArray = new String[]{"A", "B", "C", "D", "E", "F", "G", "H"};
    private static String[] rowArray = new String[]{"1", "2", "3", "4", "5", "6", "7", "8"};

    public static void main(String[] args) {
        SpringApplication.run(ChessApplication.class, args);
        Scanner in = new Scanner(System.in);
        Chessboard chessboard = new Chessboard(columnArray, rowArray, null);
        System.out.println("_____________________________________________________________");
        System.out.println("Loading chess board...");
        System.out.println("_____________________________________________________________");
        prepareChessBoard(chessboard);
        System.out.println("_____________________________________________________________");
        System.out.println("Piece Types: "+ Arrays.asList(ReferencePieceTypes.values()));
        System.out.println("_____________________________________________________________");
        startGame(in,chessboard);
        System.out.println("Game ended Thanks for playing");
    }

    protected static void prepareChessBoard(Chessboard chessboard) {
        String[][] chessboardArray = new String[rowArray.length][columnArray.length];
        for(int i=7;i>=0;i--){
            for(int j=0;j<8;j++){
                chessboardArray[i][j]=columnArray[j]+rowArray[i];
                System.out.print(chessboardArray[i][j]+" ");
            }
            System.out.println(EMPTY_STRING);
        }
        chessboard.setChessboardArray(chessboardArray);
    }

    private static void startGame(Scanner in, Chessboard chessboard) {
        boolean takeInputAgain = true;
        while (takeInputAgain) {
            System.out.println("Enter Input eg:King D5");
            String userInput = in.nextLine();
            System.out.println("You entered string " + userInput);
            identifyAndPrintPossibleMoves(userInput,chessboard);
            System.out.println("Want to try again?");
            System.out.println("Y or N");
            String yesOrNo = in.nextLine();
            takeInputAgain = null != yesOrNo && yesOrNo.equalsIgnoreCase("Y");
        }
    }

    protected static List<String> identifyAndPrintPossibleMoves(String userInput, Chessboard chessboard) {
        List<String> possibleMoves = null;
        if (null != userInput) {
            InputValidator validator = new InputValidator();
            String[] userInputArr = null != userInput?userInput.split(REGEX_SPACE):null;
            boolean isInputValid = validator.validateInput(userInputArr,chessboard);
            if (isInputValid) {
                String pieceType = userInputArr[0];
                String inputPosition = userInputArr[1];
                possibleMoves = PossibleMovesIdentifier.fetchPossibleMoves(ReferencePieceTypes.valueOf(pieceType.toUpperCase()),inputPosition,chessboard);
                System.out.println("Output -  " + possibleMoves);
            }
        }
        else {
            System.out.println("Input provided is null, which is invalid");
        }
        return possibleMoves;
    }
}