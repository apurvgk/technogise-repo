package com.chess;

import java.util.Arrays;

public class InputValidator {
    
    public boolean validateInput( String[] inputArr,Chessboard chessboard) {
        
        if (null == inputArr) {
            System.out.println("No input provided");
            System.out.println(" Please provide input in following format:<PieceType><Space><CurrentPosition> eg:King D5");
            return false;
        }
        
        if(null == chessboard){
            System.out.println("Issue Initializing application");
            return false;
        }else{
            if(null == chessboard.getRowArray() || null == chessboard.getColumnArray() || null == chessboard.getChessboardArray()){
                System.out.println("Issue Initializing application");
                return false;
            }
        }
        
        if (inputArr.length != 2) {
            System.out.println("Input provided is not correct or the input format is incorrect.");
            System.out.println(" Please provide input in following format:<PieceType><Space><CurrentPosition> eg:King D5");
            return false;
        }
        if (inputArr.length == 2) {
            try{
            String pieceType = inputArr[0];
            if (null != pieceType && null == ReferencePieceTypes.valueOf(pieceType.toUpperCase())) {
                System.out.println("Invalid Piece type provided.");
                return false;
            }}catch(IllegalArgumentException e){
                System.out.println("Invalid piece type provided.");
                return false;
            }
            
            String position = inputArr[1];
            if (null != position && position.length()==2) {
                String alphabet = position.charAt(0)+"";
                String number = position.charAt(1)+"";
               boolean isalphabetValid =  Arrays.stream(chessboard.getColumnArray()).anyMatch(c->c.equalsIgnoreCase(alphabet));
               boolean isNumberValid =  Arrays.stream(chessboard.getRowArray()).anyMatch(c->c.equalsIgnoreCase(number));
               if(!isalphabetValid || !isNumberValid){
                   System.out.println("Invalid Position provided.");
                   return false;
               }
            }else{
                System.out.println("Invalid Position provided.");
                return false;
            }
        }
        return true;
    }
}