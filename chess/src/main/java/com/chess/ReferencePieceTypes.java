package com.chess;

import java.util.Arrays;
import java.util.List;

public enum ReferencePieceTypes {
    KING("King"),
    QUEEN("Queen"),
    BISHOP("Bishop"),
    HORSE("Horse"),
    ROOK("Rook"),
    PAWN("Pawn");

    private String pieceName;

    private ReferencePieceTypes(String pieceName) {
        this.pieceName = pieceName;
    }

    public String getPieceName() {
        return pieceName;
    }

    public void setPieceName(String pieceName) {
        this.pieceName = pieceName;
    }
    
    public static boolean isPieceTypeAvailable(String pieceType){
        List<ReferencePieceTypes> referencePieceTypes = Arrays.asList(ReferencePieceTypes.values());
        if(referencePieceTypes.stream().anyMatch(s -> s.getPieceName().equalsIgnoreCase(pieceType))){
            return true;
        }else{
           return false;
        }
    }
}