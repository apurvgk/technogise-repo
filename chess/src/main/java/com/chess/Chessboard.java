package com.chess;

public class Chessboard {

    private String[] columnArray;

    private String[] rowArray;

    private String[][] chessboardArray;
    
    public Chessboard(String[] columnArray, String[] rowArray, String[][] chessBoard) {
        super();
        this.columnArray = columnArray;
        this.rowArray = rowArray;
        this.chessboardArray = chessBoard;
    }

    public String[] getColumnArray() {
        return columnArray;
    }

    public String[] getRowArray() {
        return rowArray;
    }

    public String[][] getChessboardArray() {
        return chessboardArray;
    }

    public void setChessboardArray(String[][] chessboardArray) {
        this.chessboardArray = chessboardArray;
    }
}